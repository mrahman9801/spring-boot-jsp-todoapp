insert into todo (ID, USERNAME, DESCRIPTION, TARGET_DATE, COMPLETED)
values(10001, 'Muhammad', 'Get AWS Certified', CURRENT_DATE(), false);

insert into todo (ID, USERNAME, DESCRIPTION, TARGET_DATE, COMPLETED)
values(10002, 'Muhammad', 'Get Azure Certified', CURRENT_DATE(), false);

insert into todo (ID, USERNAME, DESCRIPTION, TARGET_DATE, COMPLETED)
values(10003, 'Steve', 'Get Azure Certified', CURRENT_DATE(), false);