		<%@ include file="common/header.jspf" %>
		<%@ include file="common/nav.jspf" %>
		<div class="container">
<%-- 			<div>Hello, ${name }.</div>
			<hr> --%>
			<h1>Your Todos</h1>
			<table class="table">
			<thead> 
				<tr> 
					<th>Description</th>
					<th>Traget Date</th>
					<th>Completed?</th>
					<th></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${todos }" var="todo"> 
					<tr> 
						<td>${todo.description } </td>
						<td>${todo.targetDate }</td>
						
						<c:choose>
						    <c:when test="${todo.completed=='false'}">
						        <td> <a href="complete-todo?id=${todo.id }" class="btn btn-warning">Complete</a> </td>
						        <br />
						    </c:when>    
						    <c:otherwise>
						        <td>${todo.completed }</td>
						        <br />
						    </c:otherwise>
						</c:choose>
						<td> <a href="update-todo?id=${todo.id }" class="btn btn-success">Edit</a> </td>
						<td> <a href="delete-todo?id=${todo.id }" class="btn btn-danger">Delete</a> </td>
					</tr>
				</c:forEach>
			</tbody>
			</table>
			<a href="add-todo" class="btn btn-success">Add a Todo</a>
		</div>
		<%@ include file="common/footer.jspf" %>
		