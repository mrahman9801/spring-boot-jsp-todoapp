<%@ include file="common/header.jspf" %>
<%@ include file="common/nav.jspf" %>
		<div class="container">
			<h1>Welcome to the Todo app, ${name}.</h1>
			<hr>
			<div><a href="list-todos">Manage your todos</a></div>
		</div>
<%@ include file="common/footer.jspf" %>