package com.muhammad.springboot.projects.todoapp.todo;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import org.springframework.stereotype.Service;

import jakarta.validation.Valid;

@Service
public class TodoService {

	// First static hardcoded
	private static List<Todo> todos = new ArrayList<>();

	private static int todosCount = 0;

	static {
		todos.add(new Todo(++todosCount, "Muhammad", "Grocery shopping",
				LocalDate.now().plusDays(1), false));
		todos.add(new Todo(++todosCount, "Muhammad", "Get AWS Cert.",
				LocalDate.now().plusMonths(1), false));
		todos.add(new Todo(++todosCount, "Muhammad", "Apply for Master's Degree",
				LocalDate.now().plusYears(1), false));
	}

	public List<Todo> findByUsername(String username) {
		Predicate<? super Todo> predicate = todo -> todo.getUsername().contentEquals(username);
		return todos.stream().filter(predicate).toList();
	}

	public void addTodo(String username, String description,
			LocalDate targetDate, boolean completed) {
		Todo todo = new Todo(++todosCount, username, description, targetDate, false);
		todos.add(todo);
	}

	public void deleteTodoById(int id) {
		Predicate<? super Todo> predicate = todo -> todo.getId() == id;
		todos.removeIf(predicate);
	}

	public Todo findById(int id) {
		Predicate<? super Todo> predicate = todo -> todo.getId() == id;
		Todo todo = todos.stream().filter(predicate).findFirst().get();
		return todo;
	}

	public void updateTodo(@Valid Todo todo) {
		deleteTodoById(todo.getId());
		todos.add(todo);
	}

}
