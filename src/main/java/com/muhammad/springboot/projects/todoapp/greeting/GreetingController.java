package com.muhammad.springboot.projects.todoapp.greeting;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class GreetingController {

	// localhost:8081/greet
	@RequestMapping("greet")
	// This sends configures a web reponse in the form
	// of HTML. The return reponse will be plain words
	// in the body tag of the html.
	@ResponseBody
	public String greet() {
		return "Yooo, we're just checking to see if this server didn't blow up...yet. Hehe";
	}

	// JSP Views (Like Django template pages)

	@RequestMapping("greet-jsp")
	// No responsebody because we use jsps
	public String greetJsp() {
		return "greet";
	}
}
