# JavaTasks

### Get started

#### 1.  
Run the following command on terminal:

```
docker run --detach --env MYSQL_ROOT_PASSWORD=dummypassword --env MYSQL_USER=todos-user --env MYSQL_PASSWORD=dummytodos --env MYSQL_DATABASE=todos --name mysql --publish 3307:3306 mysql:8-oracle
```

If you don't have MySQL already installed, you may publish 3306:3306.  
Edit the application properties to use port 3306 instead of 3307 as well.

#### 2.  
Sample login:  
username: Steve  
password: secret  

Or make your own hardcoded logins inside:  
src/main/java/com/muhammad/springboot/projects/todoapp/security/SpringSecurityConfiguration.java
